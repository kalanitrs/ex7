import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  todo: string;
  show: boolean = false;
  todos = [];

  constructor(public authService:AuthService, private db:AngularFireDatabase) { 
  }

  ngOnInit() {
  }

  addTodo() {
    this.authService.user.subscribe(
      value => {
        let ref = this.db.database.ref('/');
        ref.child('users').child(value.uid).child('todos').child('todo').push({todo: this.todo});
        this.todo = '';
      }
    )
  }

  // showMyTasks() {
  //   this.show = true;
  //   this.authService.user.subscribe(
  //     value => {
  //       this.db.list('users/'+value.uid+'/todos').snapshotChanges().subscribe(
  //         todos => {
  //           this.todos = [];
  //           todos.forEach(
  //             todo => {
  //               let uTodos = todo.payload.toJSON();
  //               // task["$key"] = todo.key;
  //               // this.todos.push(task);
  //             }
  //           )
  //         }
  //       )
  //     }
  //   )
  // }
}
